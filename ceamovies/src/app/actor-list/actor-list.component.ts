import { Component, OnInit } from '@angular/core';
import { DataService } from '../data-service.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { ActorAndMovies } from './types/ActorAndMovies';

@Component({
  selector: 'app-actor-list',
  templateUrl: './actor-list.component.html',
  styleUrls: ['./actor-list.component.scss']
})

@Injectable()
export class ActorListComponent implements OnInit {

  allActors: any[];
  allMovies: any[];
  sharedActors: any[];

  keanuId: number;
  cageId: number;

  loading: boolean;

  resultMessage: string;

  constructor(private http: HttpClient, private dataService: DataService) { }

  ngOnInit() {
    this.loading = true;
    this.getActors();
  }

  getActors() {
    this.dataService.getActors().subscribe((data: any[]) => {
      this.allActors = data;
      this.getMovies();
    });
  }

  getMovies() {
    this.dataService.getMovies().subscribe((data: any[]) => {
      this.allMovies = data;
      this.sortOutActors();
    });
  }

  generateHeaders() {
    let headers = new HttpHeaders().set('x-chmura-cors', '7ACEF083-CEEF-424A-A135-059A48808F7B');
    return headers;
  }

  sortOutActors() {
    let ref = this;

    let keanuActorMap: Map<number, string[]> = new Map<number, string[]>();
    let cageActorMap: Map<number, string[]> = new Map<number, string[]>();
    let sharedActors: Array<any> = new Array<any>();

    let actorMap = this.allActors.reduce(function(map: Map<number, any>, actor: any) {
      map.set(actor.actorId, actor);

      if(actor.name === 'Keanu Reeves') {
        ref.keanuId = actor.actorId;
      } else if(actor.name === 'Nicolas Cage') {
        ref.cageId = actor.actorId;
      }

      return map;
    }, new Map<number, any>());

    this.allMovies.forEach(movie => {
      if(movie.actors.includes(ref.cageId)) {
        ref.addActorsToMap(movie, cageActorMap, ref.cageId);
      }

      if(movie.actors.includes(ref.keanuId)) {
        ref.addActorsToMap(movie, keanuActorMap, ref.keanuId);
      }
    });

    if(keanuActorMap.size < cageActorMap.size) {
      sharedActors = this.findCommonActors(new Set(keanuActorMap.keys()), new Set(cageActorMap.keys()), actorMap);
    } else {
      sharedActors = this.findCommonActors(new Set(cageActorMap.keys()), new Set(keanuActorMap.keys()), actorMap);
    }
    
    let jsonAnswer: Array<ActorAndMovies> = this.generateAnswerJson(sharedActors, cageActorMap, keanuActorMap, actorMap);
    this.postAnswer(jsonAnswer);
  }

  postAnswer(data: Array<ActorAndMovies>) {
    this.dataService.postAnswer(data).subscribe(result => {
      this.resultMessage = 'Results found!';
      this.sharedActors = data;
      this.loading = false;
    }, error => {
      this.resultMessage = 'The correct results were not found.';
      this.loading = false;
    });
  }

  findCommonActors(shortSet: Set<number>, longSet: Set<number>, actorMap: Map<number, any>) {
    let sharedActors: Array<any> = new Array<any>();
    shortSet.forEach(shortSetActor => {
      if(longSet.has(shortSetActor)) {
        sharedActors.push(actorMap.get(shortSetActor));
      }
    });

    return sharedActors;
  }

  addActorsToMap(movie: any, movieMap: Map<number, string[]>, actorId: number) {
    movie.actors.forEach(actor => {
      if(!movieMap.get(actor)) {
        let newValues = new Array<string>();
        newValues.push(movie.title);
        movieMap.set(actor, newValues);
      } else {
        let currentValues = movieMap.get(actor);
        currentValues.push(movie.title);
        movieMap.set(actor, currentValues);
      }
    });
  }

  generateAnswerJson(sharedActors: Array<any>, ncMovieMap: Map<number, string[]>, krMovieMap: Map<number, string[]>, actorMap: Map<number, any>) {
    let finalSolution: Array<ActorAndMovies> = new Array<ActorAndMovies>();

    sharedActors.forEach(el => {
      let temp = new ActorAndMovies();
      temp.Name = actorMap.get(el.actorId).name;
      temp.KRMovies = krMovieMap.get(el.actorId);
      temp.NCMovies = ncMovieMap.get(el.actorId);
      finalSolution.push(temp);
    });

    return finalSolution;
  }

}
