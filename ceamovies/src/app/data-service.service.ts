import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActorAndMovies } from './actor-list/types/ActorAndMovies';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getActors() {
    return this.http.get('https://ceamovies.azurewebsites.net/api/actors', {headers: this.generateHeaders()});
  }

  getMovies() {
    return this.http.get('https://ceamovies.azurewebsites.net/api/movies', {headers: this.generateHeaders()});
  }

  postAnswer(data: Array<ActorAndMovies>): Observable<any> {
    return this.http.post('https://ceamovies.azurewebsites.net/api/validation', data, {headers: this.generateHeaders()});
  }

  generateHeaders() {
    let headers = new HttpHeaders().set('x-chmura-cors', '7ACEF083-CEEF-424A-A135-059A48808F7B');
    return headers;
  }


}
